import 'package:app_continous/area.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  late Area area;
  setUp(() {
    area = Area();
  });
  
  group("Area of the circle", () {
    test("Area of the circle with radius 1 should be 3.141592", () {
      // Arrange

      // Act
      double result = area.circle(1);
      // Assert
      expect(result, 3.141592);
    });

    test("Area of the circle with radius 10 should be 3.141592", () {
      // Arrange

      // Act
      double result = area.circle(10);
      // Assert
      expect(result, 314.1592);
    });
  });

  test("Area.pi should be 3.141592", () {
    // Arrange

    // Act

    // Assert
    expect(Area.pi, 3.141592);
  });
}